#include "window.h"
#include "rect.h"
#include "texture.h"
#include <SDL2/SDL.h>
#include "log.h"

struct Window* window_ctor(
	struct Window* self, 
	const char* title, 
	int display,
	int driver,
	int width, 
	int height,
	enum WindowFlags flags)
{
	log_assert(self, "is NULL");
	log_assert(title, "is NULL");
	log_assert(display >= 0, "invalid display");
	log_assert(width   >  0, "invalid width");
	log_assert(height  >  0, "invalid height");

	int windowflags = SDL_WINDOW_SHOWN;
	int renderflags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE;
	if(flags & WINDOW_RESIZABLE)
	{
		windowflags |= SDL_WINDOW_RESIZABLE;
	}

	if(flags & WINDOW_FULLSCREEN)
	{
		windowflags |= SDL_WINDOW_FULLSCREEN;
		SDL_SetHint(SDL_HINT_VIDEO_MINIMIZE_ON_FOCUS_LOSS, "0");
	}

	if(flags & WINDOW_FULLSCREENDESKTOP)
	{
		windowflags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		SDL_SetHint(SDL_HINT_VIDEO_MINIMIZE_ON_FOCUS_LOSS, "0");
	}

	if(flags & WINDOW_VSYNC)
	{
		renderflags |= SDL_RENDERER_PRESENTVSYNC;
	}

	self->raw = SDL_CreateWindow(
		title, 
		SDL_WINDOWPOS_CENTERED_DISPLAY(display),
		SDL_WINDOWPOS_CENTERED_DISPLAY(display),
		width, 
		height, 
		windowflags
	);
	if(!self->raw)
	{
		log_error("%s", SDL_GetError());
	}

	self->renderer = SDL_CreateRenderer(self->raw, driver, renderflags);
	if(!self->renderer)
	{
		log_error("%s", SDL_GetError());

		/*
		log_warning(
			"Hardware acceleration failed (%s), trying software fallback", 
			SDL_GetError()
		);

		renderflags &= ~SDL_RENDERER_ACCELERATED; //Remove accelerated flag
		self->renderer = SDL_CreateRenderer(self->raw, driver, renderflags);
		if(!self->renderer)
		{
			log_error("%s", SDL_GetError());
		}
		*/
	}

	if(flags & WINDOW_FULLSCREEN || flags & WINDOW_FULLSCREENDESKTOP)
	{
		SDL_RenderSetLogicalSize(self->renderer, width, height);
	}

	self->title = SDL_GetWindowTitle(self->raw);
	self->flags = flags;
	self->width = width;
	self->height = height;
	self->color = color(0, 0, 0, 0);

	return self;
}

void window_setwidth(struct Window* self, int width)
{
	log_assert(self, "is NULL");
	log_assert(width > 0, "invalid size %i", width);

	self->width = width;
	SDL_SetWindowSize(self->raw, width, self->height);
}

void window_setheight(struct Window* self, int height)
{
	log_assert(self, "is NULL");
	log_assert(height > 0, "invalid size %i", height);

	self->height = height;
	SDL_SetWindowSize(self->raw, self->width, height);
}

void window_settitle(struct Window* self, const char* title)
{
	log_assert(self, "is NULL");
	log_assert(title, "is NULL");
	
	self->title = title;
	SDL_SetWindowTitle(self->raw, title);
}

void window_render(
	struct Window* self, 
	struct Texture* texture, 
	struct Rect* srect, 
	struct Rect* drect,
	double rotation)
{
	log_assert(self, "is NULL");
	log_assert(texture, "is NULL");
	log_assert(drect, "is NULL");

	SDL_Rect srect_;
	if(srect)
	{
		struct Vec2d pos = rect_getpos(srect, RECTREGPOINT_TOPLEFT);
		srect_ = (SDL_Rect){
			.x = pos.x,
			.y = pos.y,
			.w = srect->width,
			.h = srect->height
		};
	}

	struct Vec2d pos = rect_getpos(drect, RECTREGPOINT_TOPLEFT);
	SDL_Rect drect_ = {
		.x = pos.x,
		.y = pos.y,
		.w = drect->width,
		.h = drect->height
	};

	SDL_RenderCopyEx(
		self->renderer,
		texture->raw,
		srect ? &srect_ : NULL,
		&drect_,
		rotation,
		NULL,
		SDL_FLIP_NONE
	);
}

void window_clear(struct Window* self)
{
	log_assert(self, "is NULL");
	SDL_RenderClear(self->renderer);
}

void window_present(struct Window* self)
{
	log_assert(self, "is NULL");

	SDL_RenderPresent(self->renderer);
	if(SDL_GetWindowFlags(self->raw) & SDL_WINDOW_MINIMIZED)
	{
		SDL_Delay(1000 / 60); //Limit framerate while minimized (fix SDL bug)
	}
}

void window_setclearcolor(struct Window* self, struct Color color)
{
	log_assert(self, "is NULL");
	
	self->color = color;
	SDL_SetRenderDrawColor(self->renderer, color.r, color.g, color.b, color.a);
}

void window_dtor(struct Window* self)
{
	log_assert(self, "is NULL");

	SDL_DestroyRenderer(self->renderer);
	SDL_DestroyWindow(self->raw);
}

