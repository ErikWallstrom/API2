#include "../../initialize.h"
#include "../../gameloop.h"
#include "../../window.h"
#include <stdio.h>

int main(void)
{
	int display, driver;
	initialize(&display, &driver);

	struct Window window;
	window_ctor(
		&window, 
		"Game Window", 
		display, 
		driver, 
		800, 
		600, 
		WINDOW_FULLSCREENDESKTOP
	);

	gameloop_sleep(10000);
	window_dtor(&window);
	cleanup();
}
