#include <SDL2/SDL.h>
#include "../../initialize.h"
#include "../../window.h"
#include "../../log.h"
#include "../../color.h"

int main()
{
	SDL_Init(SDL_INIT_VIDEO);
	int numdisplays = SDL_GetNumVideoDisplays();
	if(numdisplays < 1)
	{
		log_error("No displays found: %s", SDL_GetError());
	}

	struct Window window;
	window_ctor(&window, "", 0, 800, 600, WINDOW_DEFAULT);

	for(int i = 0; i < numdisplays; i++)
	{
		log_info("Display %i: %s\n", i, SDL_GetDisplayName(i));
		int numdisplaymodes = SDL_GetNumDisplayModes(i);
		if(numdisplaymodes < 1)
		{
			log_error("No display modes found: %s", SDL_GetError());
		}

		for(int j = 0; j < numdisplaymodes; j++)
		{
			SDL_DisplayMode mode;
			if(SDL_GetDisplayMode(i, j, &mode))
			{
				log_error("Display mode retrieval failed %s", SDL_GetError());
			}

			log_info(
				"Mode %i: Refresh rate: %i, Resolution: %i x %i, Bpp: %i,"
					" Pixel format: %s", 
				j, 
				mode.refresh_rate, 
				mode.w, 
				mode.h, 
				SDL_BITSPERPIXEL(mode.format), 
				SDL_GetPixelFormatName(mode.format)
			);

			SDL_Rect rect;
			SDL_GetDisplayBounds(i, &rect);
			SDL_SetWindowSize(window.raw, mode.w, mode.h);
			SDL_SetWindowPosition(window.raw, rect.x, rect.y);
			window_setclearcolor(&window, color(0, 255, 255, 255));
			window_clear(&window);
			window_present(&window);
			SDL_Delay(1000);
		}

		putchar('\n');
	}

	int numdrivers = SDL_GetNumRenderDrivers();
	if(numdisplays < 1)
	{
		log_error("No display drivers found: %s", SDL_GetError());
	}

	for(int i = 0; i < numdrivers; i++)
	{
		SDL_RendererInfo info;
		if(SDL_GetRenderDriverInfo(i, &info))
		{
			log_error("Display driver retrieval failed %s", SDL_GetError());
		}

		log_info(
			"Driver %i: Name: %s, Max texture: %i x %i, Flags: %i, Num texture"
				" formats: %i", 
			i, 
			info.name, 
			info.max_texture_width, 
			info.max_texture_height,
			info.flags,
			info.num_texture_formats
		);
	}

	window_dtor(&window);
	SDL_Quit();
}

