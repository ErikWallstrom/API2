#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H

#include "inttypes.h"
#include "vec.h"
#include <SDL2/SDL_scancode.h>
#include <SDL2/SDL_keycode.h>

enum EventType
{
	EVENTTYPE_CONTROLLERAXISMOTION,
	EVENTTYPE_MOUSEMOTION,
	EVENTTYPE_MOUSEWHEEL,
	EVENTTYPE_QUIT,
};

enum ControllerAxis
{
	CONTROLLERAXIS_TRIGGERLEFT,
	CONTROLLERAXIS_TRIGGERRIGHT,
	CONTROLLERAXIS_LEFTX,
	CONTROLLERAXIS_LEFTY,
	CONTROLLERAXIS_RIGHTX,
	CONTROLLERAXIS_RIGHTY,
};

enum ControllerButton
{
	CONTROLLERBUTTON_A = 0,
	CONTROLLERBUTTON_B,
	CONTROLLERBUTTON_X,
	CONTROLLERBUTTON_Y,
	CONTROLLERBUTTON_BACK,
	CONTROLLERBUTTON_GUIDE,
	CONTROLLERBUTTON_START,
	CONTROLLERBUTTON_LEFTSTICK,
	CONTROLLERBUTTON_RIHTSTICK,
	CONTROLLERBUTTON_LEFTSHOULDER,
	CONTROLLERBUTTON_RIGHTSHOULDER,
	CONTROLLERBUTTON_UP,
	CONTROLLERBUTTON_DOWN,
	CONTROLLERBUTTON_LEFT,
	CONTROLLERBUTTON_RIGHT
};

struct Event
{
	union
	{
		struct 
		{
			enum ControllerAxis axis;
			int16_t value;
		} axismotion;

		struct
		{
			int32_t value;
		} wheelmotion;

		struct
		{
			int32_t x;
			int32_t y;
		} mousemotion;
	};
	enum EventType type;
};

enum Joystick
{
	JOYSTICK_LEFT,
	JOYSTICK_RIGHT
};

struct InputHandler
{
	Vec(void*) controllers;
	Vec(struct Event) events;
	const uint8_t* keystate;
	uint32_t mousestate;
	int mousex, mousey;
};

struct InputHandler* inputhandler_ctor(struct InputHandler* self);
int inputhandler_joystickangle(
	struct InputHandler* self, 
	size_t controller,
	enum Joystick joystick,
	double* angle
);
int inputhandler_buttondown(
	struct InputHandler* self, 
	size_t controller, 
	enum ControllerButton button
);
void inputhandler_update(struct InputHandler* self);
void inputhandler_dtor(struct InputHandler* self);

#endif
