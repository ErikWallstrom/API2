#include "inputhandler.h"
#include "log.h"
#include <SDL2/SDL.h>

struct InputHandler* inputhandler_ctor(struct InputHandler* self)
{
	log_assert(self, "is NULL");

	//TODO: Change 0 to a more appropriate value
	self->events = vec_new(struct Event, 0); 
	self->keystate = SDL_GetKeyboardState(NULL);
	self->mousestate = SDL_GetMouseState(&self->mousex, &self->mousey);
	self->controllers = vec_new(void*, 0);

	return self;
}

void inputhandler_update(struct InputHandler* self)
{
	log_assert(self, "is NULL");
	vec_clear(self->events);

	struct Event temp;
	SDL_Event event;
	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
		case SDL_CONTROLLERAXISMOTION:
			temp.type = EVENTTYPE_CONTROLLERAXISMOTION;
			temp.axismotion.value = event.caxis.value;
			switch(event.caxis.axis)
			{
			case SDL_CONTROLLER_AXIS_LEFTX:
				temp.axismotion.axis = CONTROLLERAXIS_LEFTX;
				break;

			case SDL_CONTROLLER_AXIS_RIGHTX:
				temp.axismotion.axis = CONTROLLERAXIS_RIGHTX;
				break;

			case SDL_CONTROLLER_AXIS_LEFTY:
				temp.axismotion.axis = CONTROLLERAXIS_LEFTY;
				break;

			case SDL_CONTROLLER_AXIS_RIGHTY:
				temp.axismotion.axis = CONTROLLERAXIS_RIGHTY;
				break;

			case SDL_CONTROLLER_AXIS_TRIGGERLEFT:
				temp.axismotion.axis = CONTROLLERAXIS_TRIGGERLEFT;
				break;

			case SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
				temp.axismotion.axis = CONTROLLERAXIS_TRIGGERRIGHT;
				break;

				//Handle unknown?
			}

			vec_pushback(self->events, temp);
			break;

		case SDL_MOUSEMOTION:
			temp.type = EVENTTYPE_MOUSEMOTION;
			temp.mousemotion.x = event.motion.xrel;
			temp.mousemotion.y = event.motion.yrel;
			vec_pushback(self->events, temp);
			break;

		case SDL_MOUSEWHEEL:
			temp.type = EVENTTYPE_MOUSEWHEEL;
			if(event.wheel.y)
			{
				temp.wheelmotion.value = event.wheel.y;
				vec_pushback(self->events, temp);
			}
			break;

		case SDL_QUIT:
			temp.type = EVENTTYPE_QUIT;
			vec_pushback(self->events, temp);
			break;

		case SDL_CONTROLLERDEVICEADDED:
			{
				SDL_GameController* controller = SDL_GameControllerOpen(
					event.cdevice.which
				);

				if(controller)
				{
					vec_pushback(self->controllers, controller);
					log_info(
						"Controller #%i, '%s' added", 
						event.cdevice.which,
						SDL_GameControllerNameForIndex(event.cdevice.which)
					);
				}
				else
				{
					log_warning(
						"Unable to open controller #%i, '%s' (%s)", 
						event.cdevice.which,
						SDL_GameControllerNameForIndex(event.cdevice.which),
						SDL_GetError()
					);
				}
			}
			break;

		case SDL_CONTROLLERDEVICEREMOVED:;
			//TODO
			log_warning(
				"Controller #%i, '%s' removed", 
				event.cdevice.which,
				SDL_GameControllerNameForIndex(event.cdevice.which)
			);
			break;

		default:
			break;
		}
	}

	self->mousestate = SDL_GetMouseState(&self->mousex, &self->mousey);
}

int inputhandler_joystickangle(
	struct InputHandler* self, 
	size_t controller,
	enum Joystick joystick,
	double* angle)
{
	log_assert(self, "is NULL");
	log_assert(vec_getsize(self->controllers) > controller, "is NULL");
	log_assert(angle, "is NULL");

	SDL_GameControllerAxis axisy = joystick == JOYSTICK_LEFT 
		? SDL_CONTROLLER_AXIS_LEFTX 
		: SDL_CONTROLLER_AXIS_RIGHTX;
	SDL_GameControllerAxis axisx = joystick == JOYSTICK_LEFT 
		? SDL_CONTROLLER_AXIS_LEFTY 
		: SDL_CONTROLLER_AXIS_RIGHTY;

	int16_t y = SDL_GameControllerGetAxis(self->controllers[controller], axisy);
	int16_t x = SDL_GameControllerGetAxis(self->controllers[controller], axisx);
	if(y || x)
	{
		*angle = atan2(y, x);
		return 1;
	}

	return 0;
}

int inputhandler_buttondown(
	struct InputHandler* self, 
	size_t controller, 
	enum ControllerButton button)
{
	log_assert(self, "is NULL");
	log_assert(vec_getsize(self->controllers) > controller, "is NULL");
	
	return SDL_GameControllerGetButton(self->controllers[controller], button);
}

void inputhandler_dtor(struct InputHandler* self)
{
	log_assert(self, "is NULL");

	for(size_t i = 0; i < vec_getsize(self->controllers); i++)
	{
		SDL_GameControllerClose(self->controllers[i]);
	}

	vec_delete(self->events);
}

