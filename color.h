#ifndef COLOR_H
#define COLOR_H

#include "inttypes.h"

#define color(r, g, b, a) (struct Color){r, g, b, a}

struct Color
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
};

#endif
