#include "gltexture.h"
#include "log.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#include "stb_image.h"
#include <GL/glew.h>

GLTexture* gltexture_ctor(GLTexture* self, const char* path)
{
	log_assert(self, "is NULL");
	log_assert(path, "is NULL");

	glGenTextures(1, self);
	glBindTexture(GL_TEXTURE_2D, *self);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	stbi_set_flip_vertically_on_load(1);

	int width, height;
	uint8_t* image = stbi_load(path, &width, &height, NULL, 3); //RGB
	if(!image)
	{
		log_error("Failed to load image: %s", stbi_failure_reason());
	}

	int format = GL_RGB;
	glTexImage2D(
		GL_TEXTURE_2D, 
		0, 
		format, 
		width, 
		height, 
		0, 
		format, 
		GL_UNSIGNED_BYTE,
		image
	);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(image);
	return self;
}

void gltexture_dtor(GLTexture* self)
{
	log_assert(self, "is NULL");
	glDeleteTextures(1, self);
}

