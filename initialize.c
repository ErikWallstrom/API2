#include <SDL2/SDL_ttf.h>
#include <time.h>
#include "window.h"
#include "log.h"

void initialize(int* display, int* driver)
{
	srand(time(NULL));
	SDL_version compileversion;
	SDL_version linkversion;

	SDL_VERSION(&compileversion);
	SDL_GetVersion(&linkversion);
	if(compileversion.major != linkversion.major 
		|| compileversion.minor != linkversion.minor 
		|| compileversion.patch != linkversion.patch)
	{
		log_warning(
			"Program was compiled with SDL version %i.%i.%i, but was linked"
				" with version %i.%i.%i\n",
			compileversion.major,
			compileversion.minor,
			compileversion.patch,
			linkversion.major,
			linkversion.minor,
			linkversion.patch
		);
	}

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER)) //Is this enough?
	{
		log_error("%s", SDL_GetError());
	}


	const SDL_version* ttflinkversion = TTF_Linked_Version();
	SDL_TTF_VERSION(&compileversion);
	if(compileversion.major != ttflinkversion->major 
		|| compileversion.minor != ttflinkversion->minor 
		|| compileversion.patch != ttflinkversion->patch)
	{
		log_warning(
			"Program was compiled with SDL_ttf version %i.%i.%i, but was linked"
				" with version %i.%i.%i\n",
			compileversion.major,
			compileversion.minor,
			compileversion.patch,
			ttflinkversion->major,
			ttflinkversion->minor,
			ttflinkversion->patch
		);
	}

	if(TTF_Init())
	{
		log_error("%s", TTF_GetError());
	}

	if(display)
	{
		int numdisplays = SDL_GetNumVideoDisplays();
		if(numdisplays < 1)
		{
			log_error("No displays found: %s", SDL_GetError());
		}

		printf(
			"Please choose your preferred monitor (0-%i, where 0 is default):\n"
				"\n", 
			numdisplays
		);

		for(int i = 0; i < numdisplays; i++)
		{
			printf("    %i) %s\n", i, SDL_GetDisplayName(i));
		}

		printf("\n> ");
		if(scanf("%i", display) != 1 || *display < 0 || *display >= numdisplays)
		{
			*display = 0;
			puts("\nChoosing default (0)");
		}
	}

	if(driver)
	{
		if(display)
		{
			int c; //Clear input buffer
			while ((c = getchar()) != '\n' && c != EOF) { }
		}

		int numdrivers = SDL_GetNumRenderDrivers();
		if(numdrivers < 1)
		{
			log_error("No display drivers found: %s", SDL_GetError());
		}

		printf(
			"Please choose your display driver (0-%i, where 0 is default):\n"
				"\n", 
			numdrivers
		);
		
		for(int i = 0; i < numdrivers; i++)
		{
			SDL_RendererInfo info;
			if(SDL_GetRenderDriverInfo(i, &info))
			{
				log_error("Display driver retrieval failed %s", SDL_GetError());
			}

			printf("    %i) %s\n", i, info.name);
		}

		printf("\n> ");
		if(scanf("%i", driver) != 1 || *driver < 0 || *driver >= numdrivers)
		{
			*driver = 0;
			puts("\nChoosing default (0)");
		}
	}
}

void cleanup(void)
{
	TTF_Quit();
	SDL_Quit();
}

