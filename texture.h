#ifndef TEXTURE_H
#define TEXTURE_H

#include "color.h"

//Forward declartion since font.h includes this file
struct Window;
struct Font;
struct Texture
{
	struct Color color;
	void* raw;
	int width, height;
};

struct Texture* texture_ctorimage(
	struct Texture* self, 
	const char* path,
	struct Window* window
);
struct Texture* texture_ctortext(
	struct Texture* self, 
	struct Font* font, 
	const char* text,
	struct Window* window
);
struct Texture* texture_copy(
	struct Texture* self, 
	struct Texture* dest, 
	struct Window* window
);
void texture_setcolor(struct Texture* self, struct Color color);
void texture_dtor(struct Texture* self);

#endif
