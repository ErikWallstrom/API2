#include "glwindow.h"
#include <SDL2/SDL.h>
#include "log.h"

struct GLWindow* glwindow_ctor(
	struct GLWindow* self, 
	const char* title, 
	int display, 
	int width, 
	int height,
	enum GLWindowFlags flags)
{
	log_assert(self, "is NULL");
	log_assert(title, "is NULL");
	log_assert(display >= 0, "invalid display");
	log_assert(width   >  0, "invalid width");
	log_assert(height  >  0, "invalid height");

	int windowflags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL;
	if(flags & GLWINDOW_RESIZABLE)
	{
		windowflags |= SDL_WINDOW_RESIZABLE;
	}

	if(flags & GLWINDOW_FULLSCREEN)
	{
		windowflags |= SDL_WINDOW_FULLSCREEN;
		SDL_SetHint(SDL_HINT_VIDEO_MINIMIZE_ON_FOCUS_LOSS, "0");
	}

	if(flags & GLWINDOW_FULLSCREENDESKTOP)
	{
		windowflags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		SDL_SetHint(SDL_HINT_VIDEO_MINIMIZE_ON_FOCUS_LOSS, "0");
	}

	if(flags & GLWINDOW_VSYNC)
	{
		SDL_GL_SetSwapInterval(1); //Should this be -1?
	}
	else
	{
		SDL_GL_SetSwapInterval(0);
	}

	self->raw = SDL_CreateWindow(
		title,
		SDL_WINDOWPOS_CENTERED_DISPLAY(display),
		SDL_WINDOWPOS_CENTERED_DISPLAY(display),
		width,
		height,
		windowflags
	);
	if(!self->raw)
	{
		log_error("%s", SDL_GetError());
	}

	self->context = SDL_GL_CreateContext(self->raw);
	if(!self->context)
	{
		log_error("%s", SDL_GetError());
	}

	self->title = SDL_GetWindowTitle(self->raw);
	self->flags = flags;
	self->width = width;
	self->height = height;
	self->color = color(0, 0, 0, 0);

	return self;
}

void glwindow_setwidth(struct GLWindow* self, int width)
{
	log_assert(self, "is NULL");
	log_assert(width > 0, "invalid size %i", width);

	self->width = width;
	SDL_SetWindowSize(self->raw, width, self->height);
}

void glwindow_setheight(struct GLWindow* self, int height)
{
	log_assert(self, "is NULL");
	log_assert(height > 0, "invalid size %i", height);

	self->height = height;
	SDL_SetWindowSize(self->raw, self->width, height);
}

void glwindow_settitle(struct GLWindow* self, const char* title)
{
	log_assert(self, "is NULL");
	log_assert(title, "is NULL");
	
	self->title = title;
	SDL_SetWindowTitle(self->raw, title);
}

void glwindow_present(struct GLWindow* self)
{
	log_assert(self, "is NULL");

	SDL_GL_SwapWindow(self->raw);
	if(SDL_GetWindowFlags(self->raw) & SDL_WINDOW_MINIMIZED)
	{
		SDL_Delay(1000 / 60); //Limit framerate while minimized (fix SDL bug)
	}
}

void glwindow_setclearcolor(struct GLWindow* self, struct Color color)
{
	log_assert(self, "is NULL");
	
	self->color = color;
}

void glwindow_dtor(struct GLWindow* self)
{
	log_assert(self, "is NULL");

	SDL_GL_DeleteContext(self->context);
	SDL_DestroyWindow(self->raw);
}

