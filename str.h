#ifndef STR_H
#define STR_H

#include <stddef.h>
#include "ansicode.h"

struct Str
{ 
	size_t len;
	char* data;
};

struct StrSlice
{
	size_t len;
	char* data; //Data not necessarily allocated
};

#define strslice_literal(s) (struct StrSlice){.len = sizeof s - 1, .data = s}
int strslice_equal(struct StrSlice s1, struct StrSlice s2);

struct Str* str_ctor(struct Str* self, const char* str);
struct Str* str_ctorbuffer(struct Str* self, size_t len);

__attribute__((format (printf, 2, 3)))
struct Str* str_ctorfmt(struct Str* self, const char* fmt, ...);
size_t str_insert(struct Str* self, size_t index, const char* str);

__attribute__((format (printf, 3, 4)))
size_t str_insertfmt(struct Str* self, size_t index, const char* fmt, ...);
void str_append(struct Str* self, const char* str);

__attribute__((format (printf, 2, 3)))
void str_appendfmt(struct Str* self, const char* fmt, ...);
size_t str_prepend(struct Str* self, const char* str);

__attribute__((format (printf, 2, 3)))
size_t str_prependfmt(struct Str* self, const char* fmt, ...);
int str_equal(struct Str* s1, struct Str* s2);

void str_lower(struct Str* self);
void str_upper(struct Str* self);
void str_colorize(struct Str* self, struct ANSICode* color);
void str_dtor(struct Str* self);

#endif
