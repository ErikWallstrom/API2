#ifndef FONT_H
#define FONT_H

#include "vec.h"
#include "color.h"

struct Window;
struct Font
{
	struct Color color;
	Vec(struct Texture) atlas;
	void* raw;
	size_t size;
	uint32_t pixelformat;
};

struct Font* font_ctor(
	struct Font* self, 
	const char* path, 
	size_t size, 
	struct Color color,
	struct Window* window
);
void font_dtor(struct Font* self);

#endif
