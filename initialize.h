#ifndef INITIALIZE_H
#define INITIALIZE_H

void initialize(int* display, int* driver);
void cleanup(void);

#endif
